//
//  ViewController.swift
//  app1
//
//  Created by Yashar Atajan on 3/12/18.
//  Copyright © 2018 Yaxiaer Atajiang. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {

    @IBOutlet weak var dbBG: UIImageView!
    @IBOutlet weak var PowerBt: UIButton!
    @IBOutlet weak var cloudHolder: UIView!
    @IBOutlet weak var rocketImg: UIImageView!
    @IBOutlet weak var liftLb: UILabel!
    @IBOutlet weak var boomLb: UILabel!
    
    var player: AVAudioPlayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let path = Bundle.main.path(forResource: "hustle-on", ofType: "wav")!
        let url = URL(fileURLWithPath: path)
        do {
           player = try AVAudioPlayer(contentsOf: url)
            player.prepareToPlay()
        } catch let error as NSError {
            print(error.description)
        }
        
    }
    
    @IBAction func powerBtPressed(_ sender: Any) {
        cloudHolder.isHidden = false
        dbBG.isHidden = true
        PowerBt.isHidden = true
        
        player.play()
        
        UIView.animate(withDuration: 2.5, animations: {
            self.rocketImg.frame = CGRect(x: 0, y: 145, width: 375, height: 400)
        }) {(finished) in
            self.liftLb.isHidden = false
            self.boomLb.isHidden = false
        }
    }
}

